﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace EncryptDecrypt
{
    public partial class MainForm : Form
    {
        private RSACryptoServiceProvider _rsa;
        private RSAParameters _asymmetricKeyPair;
        private RSAParameters _asymmetricPublicKey;
        private byte[] _asymmetricallyEncryptedMessage;

        private byte[] _symmetricKey;
        private byte[] _symmetricInitializationVector;
        private byte[] _symmetricallyEncryptedMessage;

        public MainForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// This method simply formats a byte array for display purposes. It is not part of the signing or sign verification logic
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private static string FormatByteArray(byte[] data)
        {
            return data==null ? "No data" : data.Aggregate("", (current, d) => current + $"{d} ");
        }

        private void generateKeyPairButton_Click(object sender, EventArgs e)
        {
            _rsa = new RSACryptoServiceProvider();
            _asymmetricKeyPair = _rsa.ExportParameters(true);               // Get the full key pair (Public & Private)
            _asymmetricPublicKey = _rsa.ExportParameters(false);            // Get just the public key
            publicKey.Text = FormatByteArray(_asymmetricPublicKey.Exponent) + FormatByteArray(_asymmetricPublicKey.Modulus);
            privateKey.Text = FormatByteArray(_asymmetricKeyPair.D) + FormatByteArray(_asymmetricKeyPair.Modulus);

            encryptDecryptWithPubKeyButton.Text = @"Encrypt /w Pub Key";
        }

        private void encryptDecryptWithPubKeyButton_Click(object sender, EventArgs e)
        {
            if (encryptDecryptWithPubKeyButton.Text.StartsWith("Encrypt"))
            {
                var messageBytes = Encoding.BigEndianUnicode.GetBytes(message.Text);
                _asymmetricallyEncryptedMessage = EncryptWithPublicKey(_asymmetricPublicKey, messageBytes);
                asymEncryptDecryptMessage.Text = (_asymmetricallyEncryptedMessage == null)
                    ? "No data"
                    : FormatByteArray(_asymmetricallyEncryptedMessage);
                encryptDecryptWithPubKeyButton.Text = @"Decrypt /w Private Key";

            }
            else
            {
                var decryptedBytes = DecryptWithPrivateKey(_asymmetricKeyPair, _asymmetricallyEncryptedMessage);
                asymEncryptDecryptMessage.Text = (decryptedBytes == null) ? "No data" : Encoding.BigEndianUnicode.GetString(decryptedBytes);
                encryptDecryptWithPubKeyButton.Text = @"Encrypt /w Pub Key";
            }
        }

        private void generateSymKeyButton_Click(object sender, EventArgs e)
        {
            var aes = Aes.Create();
            if (aes == null)
            {
                MessageBox.Show(@"Could not create Symmetric Encryption/Decryption algorithm", @"Initialization Error", MessageBoxButtons.OK);
                return;
            }
            _symmetricKey = aes.Key;
            _symmetricInitializationVector = aes.IV;
            symmetricKey.Text = FormatByteArray(_symmetricKey);
            initVector.Text = FormatByteArray(_symmetricInitializationVector);

            encryptDecryptButton.Text = @"Encrypt / w Sym Key";
        }

        private void encryptDecryptButton_Click(object sender, EventArgs e)
        {
            if (encryptDecryptButton.Text.StartsWith("Encrypt"))
            {
                var messageBytes = Encoding.BigEndianUnicode.GetBytes(message.Text);
                _symmetricallyEncryptedMessage = EncryptWithSymmetricKeyAlgorithm(symmetricAlgorithm.Text,
                    _symmetricKey,
                    _symmetricInitializationVector,
                    messageBytes);
                symEncryptDecryptMessage.Text = (_symmetricallyEncryptedMessage == null)
                    ? "No data"
                    : FormatByteArray(_symmetricallyEncryptedMessage);
                encryptDecryptButton.Text = @"Decrypt / w Sym Key";
            }
            else
            {
                var decryptedBytes = DecryptWithSymmetricKeyAlgorithm(symmetricAlgorithm.Text,
                    _symmetricKey,
                    _symmetricInitializationVector,
                    _symmetricallyEncryptedMessage);
                symEncryptDecryptMessage.Text = (decryptedBytes == null) ? "No data" : Encoding.BigEndianUnicode.GetString(decryptedBytes);
                encryptDecryptButton.Text = @"Encrypt / w Sym Key";
            }
        }

        /// <summary>
        /// Encrypt using a symmetric-key algorithm
        /// 
        /// Note that this method is intentionally written not use any data members of the this class (and therefore as a static method)
        /// show how the necessary information would be passed into the encryption functionality.
        /// </summary>
        /// <param name="algorithmName">The name of the encryption algorithm to use</param>
        /// <param name="symmetricKey">The symmetric-key to use</param>
        /// <param name="initializationVector">The initialization vector to use</param>
        /// <param name="messageBytes">The message (as a byte array)</param>
        /// <returns></returns>
        private static byte[] EncryptWithSymmetricKeyAlgorithm(string algorithmName,
            byte[] symmetricKey,
            byte[] initializationVector,
            byte[] messageBytes)
        {
            byte[] encryptedMessageBytes = null;
            try
            {
                var encryptionAlgorithm = Aes.Create(algorithmName);
                if (encryptionAlgorithm == null)
                {
                    MessageBox.Show(@"Could not create Symmetric Encryption/Decryption algorithm", @"Initialization Error", MessageBoxButtons.OK);
                    return null;
                }
                var encryptor = encryptionAlgorithm.CreateEncryptor(symmetricKey, initializationVector);
                encryptedMessageBytes = encryptor.TransformFinalBlock(messageBytes, 0, messageBytes.Length);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, @"Encryption Error", MessageBoxButtons.OK);
            }

            return encryptedMessageBytes;
        }

        /// <summary>
        /// Decrypt using a symmetric-key algorithm
        /// 
        /// Note that this method is intentionally written not use any data members of the this class (and therefore as a static method)
        /// show how the necessary information would be passed into the encryption functionality.
        /// </summary>
        /// <param name="algorithmName">The name of the encryption algorithm to use</param>
        /// <param name="symmetricKey">The symmetric-key to use</param>
        /// <param name="initializationVector">The initialization vector to use</param>
        /// <param name="encryptedBytes">The encoded message bytes (as a byte array)</param>
        /// <returns></returns>
        private static byte[] DecryptWithSymmetricKeyAlgorithm(string algorithmName,
            byte[] symmetricKey,
            byte[] initializationVector,
            byte[] encryptedBytes)
        {
            byte[] messageBytes = null;
            try
            {
                var encryptionAlgorithm = Aes.Create(algorithmName);
                if (encryptionAlgorithm == null)
                {
                    MessageBox.Show(@"Could not create Symmetric Encryption/Decryption algorithm", @"Initialization Error", MessageBoxButtons.OK);
                    return null;
                }
                var decryptor = encryptionAlgorithm.CreateDecryptor(symmetricKey, initializationVector);

                messageBytes = decryptor.TransformFinalBlock(encryptedBytes, 0, encryptedBytes.Length);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, @"Encryption Error", MessageBoxButtons.OK);
            }

            return messageBytes;
        }

        /// <summary>
        /// Encrypt using a public-key and an RSA algorithm
        /// 
        /// Note that this method is intentionally written not use any data members of the this class (and therefore as a static method)
        /// show how the necessary information would be passed into the encryption functionality.
        /// </summary>
        /// <param name="publicKey">The RSA parameter object containing the public key</param>
        /// <param name="messageBytes">The message (as a byte array)</param>
        /// <returns></returns>
        private static byte[] EncryptWithPublicKey(
            RSAParameters publicKey,
            byte[] messageBytes)
        {
            byte[] encryptedMessageBytes = null;
            try
            {
                var rsa = new RSACryptoServiceProvider();
                rsa.ImportParameters(publicKey);
                encryptedMessageBytes = rsa.Encrypt(messageBytes, false);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, @"Encryption Error", MessageBoxButtons.OK);
            }

            return encryptedMessageBytes;
        }

        /// <summary>
        /// Decrypt using a private-key RSA algorithm
        /// 
        /// Note that this method is intentionally written not use any data members of the this class (and therefore as a static method)
        /// show how the necessary information would be passed into the encryption functionality.
        /// </summary>
        /// <param name="fullKeyPair">The full key price</param>
        /// <param name="encryptedBytes">The encoded message bytes (as a byte array)</param>
        /// <returns></returns>
        private static byte[] DecryptWithPrivateKey(
            RSAParameters fullKeyPair,
            byte[] encryptedBytes)
        {
            byte[] messageBytes = null;
            try
            {
                var rsa = new RSACryptoServiceProvider();
                rsa.ImportParameters(fullKeyPair);
                messageBytes = rsa.Decrypt(encryptedBytes, false);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, @"Encryption Error", MessageBoxButtons.OK);
            }

            return messageBytes;
        }

    }
}
