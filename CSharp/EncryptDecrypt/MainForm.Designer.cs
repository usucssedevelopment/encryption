﻿namespace EncryptDecrypt
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.generateKeyPairButton = new System.Windows.Forms.Button();
            this.generateSymKeyButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.privateKey = new System.Windows.Forms.TextBox();
            this.publicKey = new System.Windows.Forms.TextBox();
            this.publicKeyLabel = new System.Windows.Forms.Label();
            this.privateKeyLabel = new System.Windows.Forms.Label();
            this.symmetricKey = new System.Windows.Forms.TextBox();
            this.messageLabel = new System.Windows.Forms.Label();
            this.symmetricKeyLabel = new System.Windows.Forms.Label();
            this.message = new System.Windows.Forms.TextBox();
            this.encryptDecryptWithPubKeyButton = new System.Windows.Forms.Button();
            this.asymEncryptDecryptMessage = new System.Windows.Forms.TextBox();
            this.symEncryptDecryptMessage = new System.Windows.Forms.TextBox();
            this.encryptDecryptButton = new System.Windows.Forms.Button();
            this.symmetricAlgorithmLabel = new System.Windows.Forms.Label();
            this.symmetricAlgorithm = new System.Windows.Forms.ComboBox();
            this.initVectorLabel = new System.Windows.Forms.Label();
            this.initVector = new System.Windows.Forms.TextBox();
            this.asymmetricAlgorithm = new System.Windows.Forms.ComboBox();
            this.asymmetricAlgorithmLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // generateKeyPairButton
            // 
            this.generateKeyPairButton.Location = new System.Drawing.Point(401, 100);
            this.generateKeyPairButton.Name = "generateKeyPairButton";
            this.generateKeyPairButton.Size = new System.Drawing.Size(128, 23);
            this.generateKeyPairButton.TabIndex = 0;
            this.generateKeyPairButton.Text = "Generate Key Pair";
            this.generateKeyPairButton.UseVisualStyleBackColor = true;
            this.generateKeyPairButton.Click += new System.EventHandler(this.generateKeyPairButton_Click);
            // 
            // generateSymKeyButton
            // 
            this.generateSymKeyButton.Location = new System.Drawing.Point(401, 422);
            this.generateSymKeyButton.Name = "generateSymKeyButton";
            this.generateSymKeyButton.Size = new System.Drawing.Size(128, 23);
            this.generateSymKeyButton.TabIndex = 1;
            this.generateSymKeyButton.Text = "Generate Sym Key";
            this.generateSymKeyButton.UseVisualStyleBackColor = true;
            this.generateSymKeyButton.Click += new System.EventHandler(this.generateSymKeyButton_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 386);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(843, 23);
            this.label1.TabIndex = 2;
            this.label1.Text = "Symmetric Encryption";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(0, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(843, 23);
            this.label2.TabIndex = 3;
            this.label2.Text = "Asymmetric Encryption";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // privateKey
            // 
            this.privateKey.Enabled = false;
            this.privateKey.Location = new System.Drawing.Point(85, 244);
            this.privateKey.Multiline = true;
            this.privateKey.Name = "privateKey";
            this.privateKey.Size = new System.Drawing.Size(444, 96);
            this.privateKey.TabIndex = 8;
            // 
            // publicKey
            // 
            this.publicKey.Enabled = false;
            this.publicKey.Location = new System.Drawing.Point(85, 142);
            this.publicKey.Multiline = true;
            this.publicKey.Name = "publicKey";
            this.publicKey.Size = new System.Drawing.Size(444, 96);
            this.publicKey.TabIndex = 7;
            // 
            // publicKeyLabel
            // 
            this.publicKeyLabel.AutoSize = true;
            this.publicKeyLabel.Location = new System.Drawing.Point(19, 145);
            this.publicKeyLabel.Name = "publicKeyLabel";
            this.publicKeyLabel.Size = new System.Drawing.Size(60, 13);
            this.publicKeyLabel.TabIndex = 9;
            this.publicKeyLabel.Text = "Public Key:";
            // 
            // privateKeyLabel
            // 
            this.privateKeyLabel.AutoSize = true;
            this.privateKeyLabel.Location = new System.Drawing.Point(19, 247);
            this.privateKeyLabel.Name = "privateKeyLabel";
            this.privateKeyLabel.Size = new System.Drawing.Size(64, 13);
            this.privateKeyLabel.TabIndex = 10;
            this.privateKeyLabel.Text = "Private Key:";
            // 
            // symmetricKey
            // 
            this.symmetricKey.Enabled = false;
            this.symmetricKey.Location = new System.Drawing.Point(87, 466);
            this.symmetricKey.Multiline = true;
            this.symmetricKey.Name = "symmetricKey";
            this.symmetricKey.Size = new System.Drawing.Size(442, 96);
            this.symmetricKey.TabIndex = 11;
            // 
            // messageLabel
            // 
            this.messageLabel.AutoSize = true;
            this.messageLabel.Location = new System.Drawing.Point(21, 26);
            this.messageLabel.Name = "messageLabel";
            this.messageLabel.Size = new System.Drawing.Size(53, 13);
            this.messageLabel.TabIndex = 12;
            this.messageLabel.Text = "Message:";
            // 
            // symmetricKeyLabel
            // 
            this.symmetricKeyLabel.AutoSize = true;
            this.symmetricKeyLabel.Location = new System.Drawing.Point(21, 469);
            this.symmetricKeyLabel.Name = "symmetricKeyLabel";
            this.symmetricKeyLabel.Size = new System.Drawing.Size(54, 13);
            this.symmetricKeyLabel.TabIndex = 13;
            this.symmetricKeyLabel.Text = "Sym. Key:";
            // 
            // message
            // 
            this.message.Location = new System.Drawing.Point(87, 23);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(732, 20);
            this.message.TabIndex = 14;
            this.message.Text = "Dummy message";
            // 
            // encryptDecryptWithPubKeyButton
            // 
            this.encryptDecryptWithPubKeyButton.Location = new System.Drawing.Point(565, 100);
            this.encryptDecryptWithPubKeyButton.Name = "encryptDecryptWithPubKeyButton";
            this.encryptDecryptWithPubKeyButton.Size = new System.Drawing.Size(80, 36);
            this.encryptDecryptWithPubKeyButton.TabIndex = 17;
            this.encryptDecryptWithPubKeyButton.Text = "Encrypt /w Pub Key";
            this.encryptDecryptWithPubKeyButton.UseVisualStyleBackColor = true;
            this.encryptDecryptWithPubKeyButton.Click += new System.EventHandler(this.encryptDecryptWithPubKeyButton_Click);
            // 
            // asymEncryptDecryptMessage
            // 
            this.asymEncryptDecryptMessage.Enabled = false;
            this.asymEncryptDecryptMessage.Location = new System.Drawing.Point(565, 142);
            this.asymEncryptDecryptMessage.Multiline = true;
            this.asymEncryptDecryptMessage.Name = "asymEncryptDecryptMessage";
            this.asymEncryptDecryptMessage.Size = new System.Drawing.Size(254, 198);
            this.asymEncryptDecryptMessage.TabIndex = 19;
            // 
            // symEncryptDecryptMessage
            // 
            this.symEncryptDecryptMessage.Enabled = false;
            this.symEncryptDecryptMessage.Location = new System.Drawing.Point(565, 466);
            this.symEncryptDecryptMessage.Multiline = true;
            this.symEncryptDecryptMessage.Name = "symEncryptDecryptMessage";
            this.symEncryptDecryptMessage.Size = new System.Drawing.Size(254, 176);
            this.symEncryptDecryptMessage.TabIndex = 24;
            // 
            // encryptDecryptButton
            // 
            this.encryptDecryptButton.Location = new System.Drawing.Point(565, 422);
            this.encryptDecryptButton.Name = "encryptDecryptButton";
            this.encryptDecryptButton.Size = new System.Drawing.Size(80, 36);
            this.encryptDecryptButton.TabIndex = 23;
            this.encryptDecryptButton.Text = "Encrypt /w Sym Key";
            this.encryptDecryptButton.UseVisualStyleBackColor = true;
            this.encryptDecryptButton.Click += new System.EventHandler(this.encryptDecryptButton_Click);
            // 
            // symmetricAlgorithmLabel
            // 
            this.symmetricAlgorithmLabel.AutoSize = true;
            this.symmetricAlgorithmLabel.Location = new System.Drawing.Point(26, 427);
            this.symmetricAlgorithmLabel.Name = "symmetricAlgorithmLabel";
            this.symmetricAlgorithmLabel.Size = new System.Drawing.Size(53, 13);
            this.symmetricAlgorithmLabel.TabIndex = 25;
            this.symmetricAlgorithmLabel.Text = "Algorithm:";
            // 
            // symmetricAlgorithm
            // 
            this.symmetricAlgorithm.FormattingEnabled = true;
            this.symmetricAlgorithm.Items.AddRange(new object[] {
            "AES",
            "AesCryptoServiceProvider",
            "AesManaged"});
            this.symmetricAlgorithm.Location = new System.Drawing.Point(87, 427);
            this.symmetricAlgorithm.Name = "symmetricAlgorithm";
            this.symmetricAlgorithm.Size = new System.Drawing.Size(224, 21);
            this.symmetricAlgorithm.TabIndex = 26;
            this.symmetricAlgorithm.Text = "AES";
            // 
            // initVectorLabel
            // 
            this.initVectorLabel.AutoSize = true;
            this.initVectorLabel.Location = new System.Drawing.Point(21, 573);
            this.initVectorLabel.Name = "initVectorLabel";
            this.initVectorLabel.Size = new System.Drawing.Size(58, 13);
            this.initVectorLabel.TabIndex = 28;
            this.initVectorLabel.Text = "Init Vector:";
            // 
            // initVector
            // 
            this.initVector.Enabled = false;
            this.initVector.Location = new System.Drawing.Point(87, 570);
            this.initVector.Multiline = true;
            this.initVector.Name = "initVector";
            this.initVector.Size = new System.Drawing.Size(442, 72);
            this.initVector.TabIndex = 27;
            // 
            // asymmetricAlgorithm
            // 
            this.asymmetricAlgorithm.FormattingEnabled = true;
            this.asymmetricAlgorithm.Items.AddRange(new object[] {
            "RSA"});
            this.asymmetricAlgorithm.Location = new System.Drawing.Point(87, 100);
            this.asymmetricAlgorithm.Name = "asymmetricAlgorithm";
            this.asymmetricAlgorithm.Size = new System.Drawing.Size(224, 21);
            this.asymmetricAlgorithm.TabIndex = 30;
            this.asymmetricAlgorithm.Text = "RSA";
            // 
            // asymmetricAlgorithmLabel
            // 
            this.asymmetricAlgorithmLabel.AutoSize = true;
            this.asymmetricAlgorithmLabel.Location = new System.Drawing.Point(26, 100);
            this.asymmetricAlgorithmLabel.Name = "asymmetricAlgorithmLabel";
            this.asymmetricAlgorithmLabel.Size = new System.Drawing.Size(53, 13);
            this.asymmetricAlgorithmLabel.TabIndex = 29;
            this.asymmetricAlgorithmLabel.Text = "Algorithm:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(843, 659);
            this.Controls.Add(this.asymmetricAlgorithm);
            this.Controls.Add(this.asymmetricAlgorithmLabel);
            this.Controls.Add(this.initVectorLabel);
            this.Controls.Add(this.initVector);
            this.Controls.Add(this.symmetricAlgorithm);
            this.Controls.Add(this.symmetricAlgorithmLabel);
            this.Controls.Add(this.symEncryptDecryptMessage);
            this.Controls.Add(this.encryptDecryptButton);
            this.Controls.Add(this.asymEncryptDecryptMessage);
            this.Controls.Add(this.encryptDecryptWithPubKeyButton);
            this.Controls.Add(this.message);
            this.Controls.Add(this.symmetricKeyLabel);
            this.Controls.Add(this.messageLabel);
            this.Controls.Add(this.symmetricKey);
            this.Controls.Add(this.privateKeyLabel);
            this.Controls.Add(this.publicKeyLabel);
            this.Controls.Add(this.privateKey);
            this.Controls.Add(this.publicKey);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.generateSymKeyButton);
            this.Controls.Add(this.generateKeyPairButton);
            this.Name = "MainForm";
            this.Text = "Encryption / Decryption Example";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button generateKeyPairButton;
        private System.Windows.Forms.Button generateSymKeyButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox privateKey;
        private System.Windows.Forms.TextBox publicKey;
        private System.Windows.Forms.Label publicKeyLabel;
        private System.Windows.Forms.Label privateKeyLabel;
        private System.Windows.Forms.TextBox symmetricKey;
        private System.Windows.Forms.Label messageLabel;
        private System.Windows.Forms.Label symmetricKeyLabel;
        private System.Windows.Forms.TextBox message;
        private System.Windows.Forms.Button encryptDecryptWithPubKeyButton;
        private System.Windows.Forms.TextBox asymEncryptDecryptMessage;
        private System.Windows.Forms.TextBox symEncryptDecryptMessage;
        private System.Windows.Forms.Button encryptDecryptButton;
        private System.Windows.Forms.Label symmetricAlgorithmLabel;
        private System.Windows.Forms.ComboBox symmetricAlgorithm;
        private System.Windows.Forms.Label initVectorLabel;
        private System.Windows.Forms.TextBox initVector;
        private System.Windows.Forms.ComboBox asymmetricAlgorithm;
        private System.Windows.Forms.Label asymmetricAlgorithmLabel;
    }
}

